import {combineReducers} from 'redux'
import breadReducer from './breads/breadReducer'
import cakeReducer from './cakes/cakeReducer'

const rootReducer = combineReducers({
    bread: breadReducer,
    cake: cakeReducer
})

export default rootReducer