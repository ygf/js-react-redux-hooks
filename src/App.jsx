import React from 'react'
import { Provider } from 'react-redux'
import BreadContainer from './BreadContainer'
import CakeContainer from './CakeContainer'
import store from './redux/store'



function App() {
    return (
        <Provider store={store}>
            <CakeContainer></CakeContainer>
            <BreadContainer></BreadContainer>
        </Provider>
    )
}

export default App
