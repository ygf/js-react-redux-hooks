import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { buyBread } from './redux'

function BreadContainer() {
    const numOfBread = useSelector(state => state.bread.numOfBread)
    const dispatch = useDispatch()
    return (
        <div>
            <p>面包数量： {numOfBread}</p>
            <button onClick={() => { dispatch(buyBread()) }}>购买面包</button>
        </div>
    )
}

export default BreadContainer