import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import buyCake from './redux/cakes/cakeActions'

function CakeContainer() {
    const numOfCakes = useSelector(state => {
        return state.cake.numOfCakes
    })
    const dispatch = useDispatch()

    return (
        <div>
            <p>蛋糕数量: {numOfCakes}</p>
            <button onClick={() => dispatch(buyCake())}>购买蛋糕</button>
        </div>
    )
}

export default CakeContainer